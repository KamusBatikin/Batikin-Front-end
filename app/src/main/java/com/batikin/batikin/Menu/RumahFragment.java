/*
 * Created by Nara Syai
 * Copyright (c) 2017 Batikin. all right reserved.
 *
 * Last modified 09/01/17 18:51
 */

package com.batikin.batikin.Menu;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.batikin.batikin.Adapter.CustomListRumah;
import com.batikin.batikin.Api.ParseJSONRumah;
import com.batikin.batikin.R;

public class RumahFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    public static final String JSON_URL = "http://batikinapp.esy.es/yii2-app/backend/web/RumahBatik.php";
    private ListView listViewRumah;
    private SwipeRefreshLayout swipeRefreshLayoutRumah;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        return inflater.inflate(R.layout.fragment_rumah, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Rumah Batik");

        swipeRefreshLayoutRumah = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipe_refresh_layout_rumah);
        swipeRefreshLayoutRumah.setColorSchemeResources(R.color.colorAccent);

        sendRequest();
        listViewRumah = (ListView) getActivity().findViewById(R.id.listViewRumah);

        swipeRefreshLayoutRumah.setOnRefreshListener(this);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        swipeRefreshLayoutRumah.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayoutRumah.setRefreshing(true);

                sendRequest();
            }
        });
    }

    /**
     * This method is called when swipe refresh is pulled down
     */
    @Override
    public void onRefresh() {
        sendRequest();
    }

    private void sendRequest(){
        // showing refresh animation before making http call
        swipeRefreshLayoutRumah.setRefreshing(true);

        StringRequest stringRequest = new StringRequest(JSON_URL,
        new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSON(response);
                // stopping swipe refresh
                swipeRefreshLayoutRumah.setRefreshing(false);
            }
        },
        new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity().getApplicationContext(), "Time Out Error", Toast.LENGTH_SHORT).show();
                // stopping swipe refresh
                swipeRefreshLayoutRumah.setRefreshing(false);
            }
        });
        
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    } 

    private void showJSON(String json){
        ParseJSONRumah pj = new ParseJSONRumah(json);
        pj.ParseJSONRumah();
        CustomListRumah cl = new CustomListRumah(getActivity(), ParseJSONRumah.namas, ParseJSONRumah.alamats);
        listViewRumah.setAdapter(cl);
    }
}
