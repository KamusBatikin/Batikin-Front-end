/*
 * Created by Nara Syai on 21/11/16 7:59
 * Copyright (c) 2016.  All right reserved.
 *
 * Last modified 04/12/16 6:07
 */

package com.batikin.batikin.Menu;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.Toast;

import com.batikin.batikin.R;

public class SettingFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        return inflater.inflate(R.layout.fragment_setting, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle(R.string.nav_view_item_4);

        RadioButton bahasa1 = (RadioButton) getActivity().findViewById(R.id.indo);
        bahasa1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                //Setting Title
                alertDialog.setTitle(R.string.confirm1);
                //Setting Dialog Massage
                alertDialog.setMessage(R.string.message1);
                //Setting positive yes button
                alertDialog.setPositiveButton(R.string.yees, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Write code here
                        Toast.makeText(getContext().getApplicationContext(), R.string.yees1, Toast.LENGTH_SHORT).show();
                    }
                });

                //Setting negatife yes button
                alertDialog.setNegativeButton(R.string.noo, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Write code here
                        Toast.makeText(getContext().getApplicationContext(), R.string.noo1, Toast.LENGTH_SHORT).show();
                    }
                });

                //Showing alert Dialog
                AlertDialog dialog = alertDialog.create();
                dialog.show();
            }
        });

        RadioButton bahasa2 = (RadioButton) getActivity().findViewById(R.id.inggris);
        bahasa2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                //Setting Title
                alertDialog.setTitle(R.string.confirm);
                //Setting Dialog Massage
                alertDialog.setMessage(R.string.message);
                //Setting positive yes button
                alertDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Write code here
                        Toast.makeText(getContext().getApplicationContext(), R.string.yes1, Toast.LENGTH_SHORT).show();
                    }
                });

                //Setting negatife yes button
                alertDialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Write code here
                        Toast.makeText(getContext().getApplicationContext(), R.string.no1, Toast.LENGTH_SHORT).show();

                    }
                });

                //Showing alert Dialog
                AlertDialog dialog = alertDialog.create();
                dialog.show();
            }
        });
    }
}
