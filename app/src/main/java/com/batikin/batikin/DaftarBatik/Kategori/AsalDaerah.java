/*
 * Created by Nara Syai
 * Copyright (c) 2017 Batikin. all right reserved.
 *
 * Last modified 19/01/17 4:09
 */

package com.batikin.batikin.DaftarBatik.Kategori;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.batikin.batikin.Adapter.CustomList;
import com.batikin.batikin.Api.ParseJSON;
import com.batikin.batikin.R;

public class AsalDaerah extends AppCompatActivity {
    public static final String JSON_URL = "http://batikinapp.esy.es/yii2-app/backend/web/AsalDaerah.php";
    private ListView listView;
    // private SwipeRefreshLayout swipeRefreshLayoutAsal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kategori_asal_daerah);
        setTitle(R.string.asald);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // swipeRefreshLayoutAsal = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout_asal);
        // swipeRefyreshLayoutAsal.setColorSchemeResources(R.color.colorAccent);

        sendRequest();
        listView = (ListView) findViewById(R.id.listView);

        // swipeRefreshLayoutAsal.setOnRefreshListener(this);

        // /**
        //  * Showing Swipe Refresh animation on activity create
        //  * As animation won't start on onCreate, post runnable is used
        //  */
        // swipeRefreshLayoutAsal.post(new Runnable() {
        //     @Override
        //     public void run() {
        //         swipeRefreshLayoutAsal.setRefreshing(true);

        //         sendRequest();
        //     }
        // });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                TextView textViewName = (TextView) view.findViewById(R.id.textViewName);
                String nama = textViewName.getText().toString();

                Intent i = new Intent(AsalDaerah.this, KategoriAsalDaerah.class);
                i.putExtra("nama_daerah", nama);
                i.putExtra("id_daerah", position);
                startActivity(i);
            }
        });
    }

    /**
     * This method is called when swipe refresh is pulled down
     */
    // @Override
    // public void onRefresh() {
    //     sendRequest();
    // }

    private void sendRequest(){
        // showing refresh animation before making http call
        // swipeRefreshLayoutAsal.setRefreshing(true);

        StringRequest stringRequest = new StringRequest(JSON_URL,
        new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSON(response);
                // stopping swipe refresh
                // swipeRefreshLayoutAsal.setRefreshing(false);
            }
        },
        new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Time Out Error", Toast.LENGTH_SHORT).show();
                // stopping swipe refresh
                // swipeRefreshLayoutAsal.setRefreshing(false);
            }
        });
        
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    } 

    private void showJSON(String json){
        ParseJSON pj = new ParseJSON(json);
        pj.parseJSON();
        CustomList cl = new CustomList(this, ParseJSON.names);
        listView.setAdapter(cl);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }
        
        // else if (id == R.id.action_search) {
        //     Toast.makeText(getApplicationContext(), "Search telah dipilih!", Toast.LENGTH_SHORT).show();
        //     return true;
        // }
        return super.onOptionsItemSelected(item);
    }

}
