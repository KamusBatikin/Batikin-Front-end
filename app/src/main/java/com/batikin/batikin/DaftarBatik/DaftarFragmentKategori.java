/*
 * Created by Nara Syai on 06/12/16 9:34
 * Copyright (c) 2016.  All right reserved.
 *
 * Last modified 11/09/15 9:40
 */

package com.batikin.batikin.DaftarBatik;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.batikin.batikin.DaftarBatik.Kategori.AsalDaerah;
import com.batikin.batikin.DaftarBatik.Kategori.Motif;
import com.batikin.batikin.DaftarBatik.Kategori.Warna;
import com.batikin.batikin.R;

public class DaftarFragmentKategori extends Fragment{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        return inflater.inflate(R.layout.fragment_daftar_kategori, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Daftar Batik");

        Button basal = (Button) getActivity().findViewById(R.id.asal);
        Button bmotif = (Button) getActivity().findViewById(R.id.motif);
        Button bwarna = (Button) getActivity().findViewById(R.id.warna);

        basal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchAsalDaerah();
            }
        });
        bmotif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchMotif();
            }
        });
        bwarna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchWarna();
            }
        });
                
    }
    private void launchAsalDaerah() {
        // launch new intent instead of loading fragment
        Intent intenta = new Intent();
        intenta.setClass(getActivity(), AsalDaerah.class);
        getActivity().startActivity(intenta);
    }
    private void launchMotif() {
        // launch new intent instead of loading fragment
        Intent intentm = new Intent();
        intentm.setClass(getActivity(), Motif.class);
        getActivity().startActivity(intentm);
    }
    private void launchWarna() {
        // launch new intent instead of loading fragment
        Intent intentw = new Intent();
        intentw.setClass(getActivity(), Warna.class);
        getActivity().startActivity(intentw);
    }
}
