/*
 * Created by Nara Syai
 * Copyright (c) 2017 Batikin. all right reserved.
 *
 * Last modified 19/01/17 4:09
 */

package com.batikin.batikin.DaftarBatik.Kategori;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.batikin.batikin.Adapter.CustomListMotif;
import com.batikin.batikin.Api.ParseJSONMotif;
import com.batikin.batikin.R;

public class Motif extends AppCompatActivity{
    String JSON_URL = "http://batikinapp.esy.es/yii2-app/backend/web/Motif.php";
    private ListView listViewMotif;
    // private SwipeRefreshLayout swipeRefreshLayoutMotif;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kategori_motif);
        setTitle(R.string.motif);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // swipeRefreshLayoutMotif = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout_motif);
        // swipeRefreshLayoutMotif.setColorSchemeResources(R.color.colorAccent);

        sendRequest();
        listViewMotif = (ListView) findViewById(R.id.listViewMotif);

        // swipeRefreshLayoutMotif.setOnRefreshListener(this);

        // /**
        //  * Showing Swipe Refresh animation on activity create
        //  * As animation won't start on onCreate, post runnable is used
        //  */
        // swipeRefreshLayoutMotif.post(new Runnable() {
        //     @Override
        //     public void run() {
        //         swipeRefreshLayoutMotif.setRefreshing(true);

        //         sendRequest();
        //     }
        // });

        listViewMotif.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                TextView textViewName = (TextView) view.findViewById(R.id.textViewNameMotif);

                String nama = textViewName.getText().toString();
                Intent i = new Intent(Motif.this, KategoriMotif.class);
                i.putExtra("nama_motif", nama);
                i.putExtra("id_motif", position);
                startActivity(i);
            }
        });
    }

    /**
     * This method is called when swipe refresh is pulled down
     */
    // @Override
    // public void onRefresh() {
    //     sendRequest();
    // }

    private void sendRequest(){

        // showing refresh animation before making http call
        // swipeRefreshLayoutMotif.setRefreshing(true);
        StringRequest stringRequest = new StringRequest(JSON_URL,
        new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSON(response);
                // stopping swipe refresh
                // swipeRefreshLayoutMotif.setRefreshing(false);
            }
        },
        new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Time Out Error", Toast.LENGTH_SHORT).show();
                // stopping swipe refresh
                // swipeRefreshLayoutMotif.setRefreshing(false);
            }
        });
        
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    } 

    private void showJSON(String json){
        ParseJSONMotif pj = new ParseJSONMotif(json);
        pj.ParseJSONMotif();
        CustomListMotif cl = new CustomListMotif(this, ParseJSONMotif.names);
        listViewMotif.setAdapter(cl);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }
        
        // else if (id == R.id.action_search) {
        //     Toast.makeText(getApplicationContext(), "Search telah dipilih!", Toast.LENGTH_SHORT).show();
        //     return true;
        // }
        return super.onOptionsItemSelected(item);
    }
}
